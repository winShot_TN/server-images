const express = require('express');
const bodyParser = require('body-parser');

// create express app
const app = express();
const https = require('https');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
// app.use(bodyParser.json())
app.use(express.static('uploads'));
// Configuring the database

app.use(bodyParser.urlencoded({extended: true}));





app.use(bodyParser.json({ limit: '10000mb' }));
app.use(bodyParser.urlencoded({ limit: '10000mb', extended: true, parameterLimit: 50000 }));

// app.use(multipart({
//     maxFieldsSize: '500GB'
// }));

// Connecting to the database


const privateKey = fs.readFileSync('/etc/letsencrypt/live/photos.orange.winshot.net/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/photos.orange.winshot.net/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/photos.orange.winshot.net/chain.pem', 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};

const httpsServer = https.createServer(credentials, app);

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes."});
});

require('./app/routes/note.routes.js')(app);



// listen for requests
app.listen(3002, () => {
    console.log("Server images 3002");
});

httpsServer.listen(443,"0.0.0.0" , function(){
    console.log('HTTPS Server running on port Server images 443');
});